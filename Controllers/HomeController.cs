﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using gateway.Models;
using Serilog;
using System.Configuration;

namespace gateway.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            //Log.Information("username == {@Poir} ", ConfigurationManager.AppSettings["username"]);
            //Log.Information("password == {@Poir} ", ConfigurationManager.AppSettings["password"]);
            //Log.Information("dbroot == {@Poir} ", ConfigurationManager.AppSettings["dbroot"]);
            //Log.Information("username == {@Poir} ", ConfigurationManager.AppSettings["bsurl"]);
            //Log.Information("logfile == {@Poir} ", ConfigurationManager.AppSettings["logfile"]);
            //Log.Information("configfile == {@Poir} ", ConfigurationManager.AppSettings["configfile"]);

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
