﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using LiteDB;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Routing;
using Newtonsoft.Json;

using Microsoft.Extensions.Configuration;
using System.Configuration;
using Serilog;
using gateway.Models;

namespace gateway.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CnfgController : ControllerBase
    {
        [HttpGet]
        [Route("/api/Cnfg/getall")]
        [ActionName("GetAll")]
        public IActionResult GetAll()
        {
            try
            {
                List<Route> Routes = new List<Route>();                
                using (var db = new LiteDatabase(ConfigurationManager.AppSettings["dbroot"]))
                {
                    var col = db.GetCollection<Route>("route");
                    foreach (var item in col.FindAll())
                    {
                        Routes.Add(item);
                    }
                }
                return Ok(Routes);
            }
            catch (Exception ex)
            {
                return Ok(ex);
            }
            
        }



        public class AjxRoutes
        {
            public string DownstreamPathTemplate { get; set; }
            public string DownstreamScheme { get; set; }
            public string UpstreamPathTemplate { get; set; }
            public string UpstreamHttpMethod { get; set; }
            public string Host { get; set; }
            public int Port { get; set; }
        }



        public class Rootobject
        {
            public List<Datum> data { get; set; }
        }

        public class Datum
        {
            public string DownstreamPathTemplate { get; set; }
            public string DownstreamScheme { get; set; }
            public string UpstreamPathTemplate { get; set; }
            public string UpstreamHttpMethod { get; set; }
            public string Host { get; set; }
            public int Port { get; set; }
            public string actions { get; set; }
        }



        [HttpGet]
        [Route("/api/Cnfg/getallajx")]
        [ActionName("GetAllajx")]
        public IActionResult GetAllajx()
        {
            try
            {
                Rootobject ooo = new Rootobject();
                ooo.data = new List<Datum>();
                using (var db = new LiteDatabase(ConfigurationManager.AppSettings["dbroot"]))
                {
                    List<Route> Routes = new List<Route>();
                    var col = db.GetCollection<Route>("route");
                    foreach (var item in col.FindAll())
                    {
                        Datum oo = new Datum();
                        oo.DownstreamPathTemplate = item.DownstreamPathTemplate;
                        oo.UpstreamPathTemplate = item.UpstreamPathTemplate;
                        oo.UpstreamHttpMethod = item.UpstreamHttpMethod[0];
                        oo.Host = item.DownstreamHostAndPorts[0].Host;
                        oo.Port = item.DownstreamHostAndPorts[0].Port;
                        oo.DownstreamScheme = item.DownstreamScheme;
                        oo.actions = "<a uk-icon='close' onclick=deleteURL('" + oo.UpstreamPathTemplate + "') ></a>";
                        ooo.data.Add(oo);
                        Routes.Add(item);
                    }
                    System.IO.File.WriteAllText(ConfigurationManager.AppSettings["configfile"], "{\"Routes\":" + JsonConvert.SerializeObject(Routes)+"}");

                    Log.Information("ROUTES UPDATED == {@routes} ", Routes);
                }
                return Ok(ooo);
            }
            catch (Exception ex)
            {
                return Ok(ex);
            }            
        }



        [HttpGet]
        [Route("/api/Cnfg/getone")]
        [ActionName("GetOne")]
        public string GetOne(string id)
        {
            return id;
        }

        [HttpPost]
        [Route("/api/Cnfg/add")]
        [ActionName("Add")]
        public IActionResult Add(Route r)
        {
            try
            {
                using (var db = new LiteDatabase(ConfigurationManager.AppSettings["dbroot"]))
                {
                    var col = db.GetCollection<Route>("route");
                    col.EnsureIndex(x => x.UpstreamPathTemplate);
                    col.Insert(r);
                }
                return Ok(r);
            }
            catch (Exception ex)
            {
                return Ok(ex);
            }           
        }


        [HttpPost]
        [Route("/api/Cnfg/set")]
        [ActionName("Set")]
        public IActionResult Set(Route r)
        {
            try
            {
                using (var db = new LiteDatabase(ConfigurationManager.AppSettings["dbroot"]))
                {
                    var col = db.GetCollection<Route>("route");
                    // col.EnsureIndex(x => x.UpstreamPathTemplate);
                    col.Update(r);
                }
                return Ok(r);
            }
            catch (Exception ex)
            {
                return Ok(ex);
            }
        }




        public class Up
        {
            public string UpstreamPathTemplate { get; set; }
        }


        [HttpPost]
        [ActionName("Delete")]
        [Route("/api/Cnfg/delete")]
        public IActionResult Delete(Up u)
        {
            try
            {
                int rs = 0;
                using (var db = new LiteDatabase(ConfigurationManager.AppSettings["dbroot"]))
                {
                    var col = db.GetCollection<Route>("route");
                    rs = col.DeleteMany(x => x.UpstreamPathTemplate == u.UpstreamPathTemplate);

                }
                return Ok(rs);
            }
            catch (Exception ex)
            {
                return Ok(ex);
            }
            
        }



        [HttpPost]
        [ActionName("SetBSurl")]
        [Route("/api/Cnfg/SetBSurl")]
        public IActionResult SetBSurl(Up u)
        {
            try
            {
                using (var db = new LiteDatabase(ConfigurationManager.AppSettings["dbroot"]))
                {
                    var col = db.GetCollection<Up>("configs");
                    col.DeleteAll();
                    Log.Information("SetBSurl == {@Poir} ", u);
                    return Ok(col.Insert(u));
                }
            }
            catch (Exception ex)
            {
                return Ok(ex);
            }
            
        }


        [HttpGet]
        [ActionName("GetBSurl")]
        [Route("/api/Cnfg/GetBSurl")]
        public IActionResult GetBSurl()
        {
            try
            {
                using (var db = new LiteDatabase(ConfigurationManager.AppSettings["dbroot"]))
                {
                    var col = db.GetCollection<Up>("configs");
                    return Ok(col.FindAll().FirstOrDefault());
                }
            }
            catch (Exception ex)
            {
                Log.Information("GetBSurl == {@Poir} ", ex);
                return Ok(ex);
            }            
        }



    }
}
