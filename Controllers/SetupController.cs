﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;
using LiteDB;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Serilog;

namespace gateway
{

    public class SetupController : Controller
    {
        public class Up
        {
            public string UpstreamPathTemplate { get; set; }
        }
        public IActionResult Index()
        {
            try
            {

                Log.Information("Setup username == {@Poir} ", ConfigurationManager.AppSettings["username"]);
                Log.Information("Setup password == {@Poir} ", ConfigurationManager.AppSettings["password"]);



                var r = HttpContext.Request.Form;                
                Log.Information("Setup r username == {@Poir} ", r["username"]);
                Log.Information("Setup r password == {@Poir} ", r["password"]);
                if (r["username"] == ConfigurationManager.AppSettings["username"] && r["password"] == ConfigurationManager.AppSettings["password"])
                {
                    ViewBag.bsurl = ConfigurationManager.AppSettings["bsurl"];
                    
                    return View();
                }
                else
                    return Redirect("Login");

            }
            catch (Exception ex )
            {
                Log.Information("Setup exception == {@Poir} ", ex);
                return Redirect("Login");
            }




        }
    }
}
